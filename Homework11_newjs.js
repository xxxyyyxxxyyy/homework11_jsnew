// Завдання
// Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення

// Після натискання на кнопку сторінка не повинна перезавантажуватись
// // Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.
//
// ___________________есть несколько проблем: 
// 1. глаз во втором Input меняется только после второго нажатия клавишей мыши;
// 2. в опере глаза в поле Input вообще не видно;
// 3. периодически, после обновления страници поля Input не реагируют на ввод паролей.
// Не совсем понимаю, что не так прописал.
let btnPass = document.querySelectorAll('.icon-password');

btnPass.forEach(function (btn) {
    btn.onclick = function () {
        let target = this.getAttribute('data-target');
        // console.log(target);
        inputPass = document.querySelector(target);

        if (inputPass.getAttribute('type') === 'password'){
            //         // alert(АУ!);
        inputPass.setAttribute('type', 'text');
        // btn.classList.replace("fa-eye", "fa-eye-slash");
        btn.classList.remove("fa-eye");
        btn.classList.add("fa-eye-slash"); 
        } else {
        inputPass.setAttribute('type', 'password')
        // btn.classList.replace("fa-eye-slash", "fa-eye");
        btn.classList.remove("fa-eye-slash");
        btn.classList.add("fa-eye");
            
        }
    }
})

const clickButton = document.querySelector(".btn")
clickButton.addEventListener("click", (event) => {
    const firstPassword = document.getElementById("js-first-password").value;
    const secondPassword = document.getElementById("js-second-password").value;
    event.preventDefault();
    if(firstPassword === "" || secondPassword === "") {
        alert("поля не могут быть пустыми")
    }
    else if (firstPassword === secondPassword) {
        alert("You are welcome")
    }
    else {
        document.querySelector('.error').innerText = "Необходимо ввести одинаковые значения"
        // вариант 2
        // document.getElementsByClassName("error")[0].style.display = "flex";
       
    }
    
    
})

